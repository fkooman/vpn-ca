#!/bin/sh

# Generate 50 keys and have them signed by the CA and time how long this takes
# for ECDSA and EdDSA.
#
# On my oldish laptop:
#
# $ sh benchmark.sh 
# ECDSA 1s
# EdDSA 0s

# build vpn-ca, if not yet done
make vpn-ca

for TYPE in ECDSA EdDSA; do
	mkdir "${TYPE}"
	CA_DIR="${TYPE}" CA_KEY_TYPE="${TYPE}" ./vpn-ca -init-ca -name "${TYPE} CA"
	START=$(date +%s)
	for i in $(seq 50); do
		CA_DIR="${TYPE}" CA_KEY_TYPE="${TYPE}" ./vpn-ca -client -name "${TYPE}-client-${i}"
	done
	END=$(date +%s)
	echo "${TYPE} $((END-START))s"
	rm -rf "${TYPE}"
done
