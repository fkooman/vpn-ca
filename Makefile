PREFIX=/usr/local

.PHONY: all fmt lint check vet clean install sloc

vpn-ca: cmd/vpn-ca/main.go
	go build $(GOBUILDFLAGS) -o $@ codeberg.org/eduVPN/vpn-ca/cmd/vpn-ca

vpn-ca.1: vpn-ca.1.scd
	scdoc < $< > $@

all: vpn-ca vpn-ca.1

fmt:
	gofumpt -w . || go fmt ./...

lint:
	golangci-lint run -E stylecheck,revive,gocritic

check:
	# https://staticcheck.dev/
	staticcheck ./...

vet:
	go vet ./...

sloc:
	tokei . || cloc .

clean:
	rm -f vpn-ca vpn-ca.1

install: all
	install -m 0755 -D vpn-ca $(DESTDIR)$(PREFIX)/bin/vpn-ca
	install -m 0644 -D vpn-ca.1 $(DESTDIR)$(PREFIX)/share/man/man1/vpn-ca.1
