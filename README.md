**Summary**: Simple CA to manage server and client certificates

**Description**: Simple CA that can be used to manage server and client 
certificates for use with e.g. TLS VPNs, 802.1X or a simple home lab with self
issued certificates.

**License**: MIT

# Introduction

Simple CA that can be used to manage server and client certificates for use
with e.g. TLS VPNs, 802.1X, or a simple home lab with self issued certificates.

# Why

We needed something more robust and simpler than 
[easy-rsa](https://github.com/OpenVPN/easy-rsa).

# Build

    $ go build -o vpn-ca codeberg.org/eduVPN/vpn-ca/cmd/vpn-ca

We also provide a `Makefile` for your convenience:

    $ make

To generate the _manpage_, make sure you have `scdoc` installed and run:

    $ make vpn-ca.1

# Usage

    $ ./vpn-ca --help

You can also look at the provided **vpn-ca**(1) _manpage_:

    $ man ./vpn-ca.1
    
If you installed `vpn-ca` through your package manager you can also open the
installed _manpage_:

    $ man 1 vpn-ca

Another option is to read the _manpage_ [source](vpn-ca.1.scd).
